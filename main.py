#!/usr/bin/env python3
import random
import argparse
import paho.mqtt.client as mqtt

def main(args):
    client = mqtt.Client("fabreader_actor_%05d" % random.randint(0, 99999))
    
    if args.user is not None:
        if args.password is None:
            print("User set, but the password not")
            exit(-1)
        client.username_pw_set(args.user, args.password)

    client.connect(args.host, args.port)
    
    new_state = args.state
    if new_state == "free":
        client.publish("fabreader/%05d/display/title" % int(args.fabreader), "Free")
    elif new_state == "inuse":
        client.publish("fabreader/%05d/display/title" % int(args.fabreader), "In Use")
    elif new_state == "tocheck":
        client.publish("fabreader/%05d/display/title" % int(args.fabreader), "To Check")
    elif new_state == "blocked":
        client.publish("fabreader/%05d/display/title" % int(args.fabreader), "Blocked")
    elif new_state == "disabled":
        client.publish("fabreader/%05d/display/title" % int(args.fabreader), "Disabled")
    elif new_state == "reserved":
        client.publish("fabreader/%05d/display/title" % int(args.fabreader), "Reserved")
    elif new_state == "raw":
        pass # Do nothing
    else:
        print("Process actor called with unknown state %s" % new_state)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", help="MQTT Server Address", required=True)
    parser.add_argument("--port", help="MQTT Server Port", nargs='?', default=1883, type=int)
    parser.add_argument("--user", help="MQTT User (optional)")
    parser.add_argument("--password", help="MQTT Password (optional)")
    parser.add_argument("--fabreader", help="FabReader ID", required=True)
    
    parser.add_argument("name", help="name of this actor as configured in bffh.dhall")

    subparsers = parser.add_subparsers(required=True, dest="state")

    parser_free = subparsers.add_parser("free")

    parser_inuse = subparsers.add_parser("inuse")
    parser_inuse.add_argument("userid", help="The user that is now using the machine")

    parser_tocheck = subparsers.add_parser("tocheck")
    parser_tocheck.add_argument("userid", help="The user that should go check the machine")

    parser_blocked = subparsers.add_parser("blocked")
    parser_blocked.add_argument("userid", help="The user that marked the machine as blocked")

    parser_disabled = subparsers.add_parser("disabled")

    parser_reserved = subparsers.add_parser("reserved")
    parser_reserved.add_argument("userid", help="The user that reserved the machine")
    
    parser_raw = subparsers.add_parser("raw")
    parser_raw.add_argument("data", help="Raw data for for this actor")

    args = parser.parse_args()
    main(args)
