# FabReader
Updates State on FabReader Display.

## Paramters
### MQTT Configuration 
* `--host` MQTT Server Address
* `--port` MQTT Server Port
* `--user` MQTT User (optional)
* `--password` MQTT Password (optional)

### FabReader Configuration
* `--fabreader` FabReader ID